﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using FuncWorks.XNA.XTiled;

namespace Castlevania
{
    public class Personagem : Microsoft.Xna.Framework.DrawableGameComponent
    {
        public enum Direcoes { Direita, Esquerda, Acima, Abaixo }
        public enum Estados { Idle, Andando, Correndo, AtaquePrimario, PuloReto, PuloInclinado, Caindo, Parando, Pulo_PontoMax, Agachado, Observando, SuperPulo, Morto, DanoEqueda, ColisaoHorizontal, ColisaoVertical, Rasteira , EndGame }

        public Color[] textureData;

        SpriteBatch spriteBatch;
        public Rectangle spriteBatchBoundings;
        public SpriteFont FonteVida;
        public bool Pulou;
        public bool PulouMaisAlto;
        public bool PontoMaisAlto;
        public Texture2D Textura;
        private Texture2D TexturaAtaque1;
        private Texture2D TexturaCorrendo;
        private Texture2D TexturaAndando;
        private Texture2D TexturaIddle;
        private Texture2D TexturaObservando;
        private Texture2D TexturaAgachado;
        private Texture2D TexturaParando;
        private Texture2D TexturaSuperPulo;
        private Texture2D TexturaPuloReto;
        private Texture2D TexturaPuloInclinado;
        private Texture2D TexturaCaindo;
        private Texture2D TexturaPulo_PontoMax;
        public Texture2D TexturaMorte;
        private Texture2D TexturaRasteira;
        private Texture2D TexturaDanoEQueda;
        private Texture2D ColisaoVertical;
        private Texture2D ColisaoHorizontal;
        private Texture2D TexturaEndGame;
        public Vector2 Posicao;
        public Vector2 Velocidade;
        public Estados Estado;
        public Estados EstadoAnterior;
        private Vector2 Frame;
        private Vector2 FrameAnterior;
        public Direcoes Direcao;
        public int Vida;
        public Rectangle rectangle;
        public Rectangle BoundingBox;
        public Rectangle Richter_BoundingBox;
        public Vector2 Tamanho;
        private SoundEffect SomColisao;
        private SoundEffect SomMorte1;
        private SoundEffect SomMorte2;
        private SoundEffect SomCorte;
        private SoundEffect SomAtaque1;
        private SoundEffect SomAtaque2;
        private SoundEffect SofreuDano1;
        private SoundEffect SofreuDano2;
        private SoundEffect SomAcaoPulo;
        private SoundEffect Risada;
        private SoundEffect SomRasteira;
        private SoundEffect SomTeleport;
        public bool Atacando;
        public Vector2 OrigemPulo;
        public bool Pulando;
        public bool PulandoAlto;
        public bool TerminouAtaque;
        public bool PulouReto;
        public bool PulouInclinado;
        public bool SuperPulo;
        public Vector2 OrigemPosicao;
        public bool GolpeRasteiro;
        public bool Esta_no_solo;
        public bool levoudano;
        public bool Esta_morto;
        public bool colidiu_no_comeco;
        public bool colidiu_no_fim;
        public Rectangle BondingBoxAtaque;
        public Vector2 PontoAtaque;
        public Vector2 BarraVida;
        public Rectangle Area_Barra_de_vida;
        public Texture2D Life_bar;
        public Vector2 Posicao_barra_life;
        public Vector2 Posicao_contador_HP;
        //float gravidade;

        public Personagem(Game game)
            : base(game)
        {
            this.Posicao = new Vector2(100, 420);
            this.Velocidade = new Vector2(1f, 1f);
            this.Tamanho = new Vector2(40, 50);
            this.PontoAtaque = new Vector2(Posicao.X + 60, Posicao.Y);
            this.Estado = Estados.Idle;
            this.Frame = new Vector2(0, 0);
            this.Direcao = Direcoes.Direita;
            this.Vida = 100;
            this.Pulou = false;
            this.BoundingBox = new Rectangle((int)Posicao.X, (int)Posicao.Y, (int)Tamanho.X, (int)Tamanho.Y);
            //this.BondingBoxAtaque = new Rectangle((int)Posicao.X + 50, (int)Posicao.Y - 50, (int)PontoAtaque.X, (int)PontoAtaque.Y);
            this.Posicao_barra_life = new Vector2(Posicao.X - 10, Posicao.Y - 20);
            this.Posicao_contador_HP = new Vector2(Posicao.X - 10, Posicao.Y - 20);
        }

        public Personagem(Game game, Vector2 posicao)
            : base(game)
        {
            this.Posicao = posicao;
            this.Velocidade = new Vector2(1f, 1f);
            this.Tamanho = new Vector2(40, 50);
            this.PontoAtaque = new Vector2(Posicao.X + 50, Posicao.Y - 50);
            this.Estado = Estados.Idle;
            this.Frame = new Vector2(0, 0);
            this.Direcao = Direcoes.Direita;
            this.Vida = 100;
            this.Pulou = true;
            this.BoundingBox = new Rectangle((int)Posicao.X, (int)Posicao.Y, (int)Tamanho.X, (int)Tamanho.Y);
            //this.BondingBoxAtaque = new Rectangle((int)Posicao.X + 50, (int)Posicao.Y - 50, (int)PontoAtaque.X, (int)PontoAtaque.Y);
            this.Posicao_barra_life = new Vector2(Posicao.X - 10, Posicao.Y - 20);
            this.Posicao_contador_HP = new Vector2(Posicao.X - 10, Posicao.Y - 20);
        }

        public override void Initialize()
        {
            colidiu_no_comeco = false;
            colidiu_no_fim = false;
            Esta_no_solo = false;
            GolpeRasteiro = false;
            PontoMaisAlto = false;
            Atacando = false;
            Pulando = false;
            PulandoAlto = false;
            TerminouAtaque = false;
            //gravidade = 400.0f;
            //Velocidade = new Vector2(0.0f, 0.0f);
            Esta_morto = false;
            OrigemPosicao = Posicao;
            base.Initialize();
        }

        public void LoadContent(Game game)
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);
            spriteBatchBoundings = GraphicsDevice.Viewport.Bounds;
            LoadTexturesRichter();
            LoadSfxRichter();
            textureData = new Color[this.Textura.Width * Textura.Height];
            Textura.GetData(textureData);

            base.LoadContent();
        }

        public override void Update(GameTime gameTime)
        {
            if (Vida >= 0)
                Posicao += Velocidade;
            Estados_Richter();
            this.BoundingBox = new Rectangle((int)Posicao.X, (int)Posicao.Y, (int)Tamanho.X, (int)Tamanho.Y);
            this.BondingBoxAtaque = new Rectangle((int)PontoAtaque.X, (int)PontoAtaque.Y, (int)PontoAtaque.X, (int)PontoAtaque.Y);
            this.PontoAtaque = new Vector2(Posicao.X + 60, Posicao.Y);
            this.Posicao_barra_life = new Vector2(Posicao.X - 10, Posicao.Y - 20);
            this.Posicao_contador_HP = new Vector2(Posicao.X - 10, Posicao.Y - 20);
            base.Update(gameTime);
        }

        public void Draw(GameTime gameTime, SpriteBatch spriteBatch, Rectangle mapView)
        {
            spriteBatch.Draw(this.Textura, Map.Translate(Posicao, mapView), new Rectangle((int)this.Frame.X, (int)this.Frame.Y, (int)this.Tamanho.X, (int)this.Tamanho.Y), Game1.playerColor,
            0f, Vector2.Zero, 1f, this.Direcao == Direcoes.Direita ? SpriteEffects.None : SpriteEffects.FlipHorizontally, 0f);

            spriteBatch.Draw(this.Life_bar, Map.Translate(this.Posicao_barra_life, mapView), Color.White); // Desenha a barra de vida     
            spriteBatch.DrawString(this.FonteVida, this.Vida.ToString(), Map.Translate(this.Posicao_contador_HP, mapView), Color.White); // valor do HP
            //  spriteBatch.End();

            base.Draw(gameTime);
        }

        //========================================================================================================================================================
        //Acoes de Richter

        public void Atacar()
        {
            this.Atacando = true;
            this.Textura = this.TexturaAtaque1;
            this.EstadoAnterior = this.Estado;
            this.FrameAnterior = this.Frame;
            this.Estado = Estados.AtaquePrimario;
            this.Tamanho.X = 150;
            this.Tamanho.Y = 69;
            //this.Posicao.X -= 5;
            //this.Posicao.Y -= 2;
            //this.TerminouAtaque = true;
            //this.SomAtaque1.Play();
            this.BondingBoxAtaque = new Rectangle((int)PontoAtaque.X, (int)PontoAtaque.Y, (int)PontoAtaque.X, (int)PontoAtaque.Y);
            //this.Posicao.X = Posicao.
            //TerminouAtaque = true;
            // em relação aos outros sprites, o ataque deve-se deslocar 35 para traz para ficar igual, talvez 10 em y
        }

        public void Correr()
        {
            this.Textura = this.TexturaCorrendo;
            this.Estado = Estados.Correndo;
            this.Tamanho.X = 50;
            this.Tamanho.Y = 50;
            this.MudarVelocidade(10);

            switch (this.Direcao)
            {
                case Direcoes.Direita:
                    this.Posicao.X += this.Velocidade.X;
                    break;
                case Direcoes.Esquerda:
                    this.Posicao.X -= this.Velocidade.X;
                    break;
            }
            this.MudarVelocidade(1);

        }
        public void Andar()
        {
            this.Textura = this.TexturaAndando;
            this.Estado = Estados.Andando;
            this.Tamanho.X = 40;
            this.Tamanho.Y = 49;
            this.MudarVelocidade(3f);

            switch (this.Direcao)
            {
                case Direcoes.Direita:
                    this.Posicao.X += this.Velocidade.X;
                    break;
                case Direcoes.Esquerda:
                    this.Posicao.X -= this.Velocidade.X;
                    break;
            }
            this.MudarVelocidade(1f);
        }

        public void Rasteira()
        {
            this.Textura = this.TexturaRasteira;
            this.Estado = Estados.Rasteira;
            this.Tamanho.X = 100;
            this.Tamanho.Y = 55;
            this.MudarVelocidade(3);

            switch (this.Direcao)
            {
                case Direcoes.Direita:
                    this.Posicao.X += this.Velocidade.X;
                    //this.Posicao.X = this.Posicao.X + 2;
                    this.SomRasteira.Play();
                    break;
                case Direcoes.Esquerda:
                    this.Posicao.X -= this.Velocidade.X;
                    this.SomRasteira.Play();
                    break;
            }
            this.MudarVelocidade(1);

        }

        public void Colidindo_na_horizontal()
        {
            this.Textura = this.ColisaoHorizontal;
            this.Estado = Estados.ColisaoHorizontal;
            this.Tamanho.X = 40;
            this.Tamanho.Y = 49;

            Velocidade.X = -Velocidade.X;
            Posicao.X = -Posicao.X;
            if (this.Direcao == Direcoes.Esquerda)
                colidiu_no_comeco = true;
            else if (this.Direcao == Direcoes.Direita)
                colidiu_no_fim = true;

            colidiu_no_comeco = false;
            colidiu_no_fim = false;
        }

        public void Colidindo_na_vertical()
        {
            this.Textura = this.ColisaoHorizontal;
            this.Estado = Estados.ColisaoHorizontal;
            this.Tamanho.X = 40;
            this.Tamanho.Y = 49;

            Velocidade.Y = -Velocidade.Y;
            Posicao.Y = -Posicao.Y;
            this.SomColisao.Play();
            Esta_no_solo = true;
        }

        public void Movimento(Direcoes direcao)
        {
            this.Direcao = direcao;
        }

        public void Parando()
        {
            this.Textura = this.TexturaParando;
            this.Estado = Estados.Parando;
            this.Tamanho.X = 37;
            this.Tamanho.Y = 50;
        }

        public void Parado()
        {
            // if (this.TerminouAtaque == true) ;
            // this.Posicao.X += 35;
            this.MudarVelocidade(0);
            this.Textura = this.TexturaIddle;
            this.Estado = Estados.Idle;
            this.Tamanho.X = 40;
            this.Tamanho.Y = 50;
        }

        public void Agachado(Estados agachado)
        {
            this.Textura = this.TexturaAgachado;
            this.Estado = Estados.Agachado;
            this.Tamanho.X = 39;
            this.Tamanho.Y = 50;
        }
        public void Caindo()
        {
            this.Textura = this.TexturaCaindo;
            this.Estado = Estados.Caindo;
            this.Tamanho.X = 53;
            this.Tamanho.Y = 72;
        }
        public void PuloReto()
        {
            this.Textura = this.TexturaPuloReto;
            this.Estado = Estados.PuloReto;
            this.Tamanho.X = 40;
            this.Tamanho.Y = 52;

        }
        public void PuloInclinado()
        {
            this.Textura = this.TexturaPuloInclinado;
            this.Estado = Estados.PuloInclinado;
            this.Tamanho.X = 40;
            this.Tamanho.Y = 55;
            this.MudarVelocidade(2);
            switch (this.Direcao)
            {
                case Direcoes.Direita:
                    this.Posicao.X += this.Velocidade.X;
                    break;
                case Direcoes.Esquerda:
                    this.Posicao.X -= this.Velocidade.X;
                    this.MudarVelocidade(-2);
                    break;
            }
        }
        public void Pulo_PontoMax()
        {
            this.Textura = this.TexturaPulo_PontoMax;
            this.Estado = Estados.Pulo_PontoMax;
            this.Tamanho.X = 49;
            this.Tamanho.Y = 65;
        }
        public void Observando(Estados Obervando)
        {
            this.Textura = this.TexturaObservando;
            this.Estado = Estados.Observando;
            this.Tamanho.X = 40;
            this.Tamanho.Y = 49;
        }
        public void SuperJump()
        {
            this.Textura = this.TexturaSuperPulo;
            this.Estado = Estados.SuperPulo;
            this.Tamanho.X = 40;
            this.Tamanho.Y = 54;
            switch (this.Direcao)
            {
                case Direcoes.Direita:
                    this.Posicao.X += this.Velocidade.X;
                    break;
                case Direcoes.Esquerda:
                    this.Posicao.X -= this.Velocidade.X;
                    this.MudarVelocidade(-2);
                    break;
            }
        }

        public void MudarVelocidade(float v)
        {
            this.Velocidade.X = v;
        }

        public void SofrerDano(int d)
        {
            levoudano = true;
            this.Textura = this.TexturaDanoEQueda;
            this.Estado = Estados.DanoEqueda;
            this.Tamanho.X = 49;
            this.Tamanho.Y = 51;
            this.Vida -= d;
            SofreuDano1.Play();
            this.Posicao.X = this.Posicao.X - 4;
            levoudano = false;
        }

        public void Morto()
        {
            this.Textura = this.TexturaMorte;
            this.Estado = Estados.Morto;
            this.Tamanho.X = 53;
            this.Tamanho.Y = 76;
            this.SomMorte1.Play();
            this.Posicao.X = this.Posicao.X - 5;
            this.Posicao.Y = this.Posicao.Y - 5;
            Esta_morto = true;

        }
        public void EndGame_Reset()
        {
            this.Textura = this.TexturaEndGame;
            this.Estado = Estados.EndGame;
            this.Tamanho.X = 41;
            this.Tamanho.Y = 50;
            this.SomTeleport.Play();

        }
        //===================================================================================================================================================================================================
        public void LoadTexturesRichter()
        {
            this.TexturaAndando = Game.Content.Load<Texture2D>("sprites/richter_Walking");
            this.ColisaoHorizontal = Game.Content.Load<Texture2D>("sprites/richter_Walking");
            this.TexturaAtaque1 = Game.Content.Load<Texture2D>("sprites/Richter_Atk4");
            this.TexturaCorrendo = Game.Content.Load<Texture2D>("sprites/Richter_Run");
            this.TexturaIddle = Game.Content.Load<Texture2D>("sprites/richter_Iddle");
            this.TexturaPuloReto = Game.Content.Load<Texture2D>("sprites/Richter_JumpLinear");
            this.TexturaPuloInclinado = Game.Content.Load<Texture2D>("sprites/Richter_Jump2");
            this.TexturaCaindo = Game.Content.Load<Texture2D>("sprites/Richter_Down");
            this.TexturaObservando = Game.Content.Load<Texture2D>("sprites/Richter_Stay_up");
            this.TexturaParando = Game.Content.Load<Texture2D>("sprites/Richter_Stop_Run");
            this.TexturaAgachado = Game.Content.Load<Texture2D>("sprites/Richter_Crouched");
            this.TexturaPulo_PontoMax = Game.Content.Load<Texture2D>("sprites/Richter_JumpMax");
            this.TexturaSuperPulo = Game.Content.Load<Texture2D>("sprites/Richter_Super_Jump");
            this.TexturaMorte = Game.Content.Load<Texture2D>("sprites/Richter_death");
            this.TexturaDanoEQueda = Game.Content.Load<Texture2D>("sprites/Richter_dano_queda");
            this.TexturaRasteira = Game.Content.Load<Texture2D>("sprites/Richter_Rasteira");
            this.TexturaEndGame = Game.Content.Load<Texture2D>("sprites/richter_PosEnd");
            this.Textura = this.TexturaIddle;
            this.Life_bar = Game.Content.Load<Texture2D>("sprites/bar_life");
            this.FonteVida = Game.Content.Load<SpriteFont>("sprites/Vida");

        }

        public void LoadSfxRichter()
        {
            this.SomColisao = Game.Content.Load<SoundEffect>("sfx/queda de pulo");
            this.SomMorte1 = Game.Content.Load<SoundEffect>("sfx/richter_die");
            this.SomMorte2 = Game.Content.Load<SoundEffect>("sfx/richter_di2");
            this.SomCorte = Game.Content.Load<SoundEffect>("sfx/richter_golpe");
            this.SomAtaque1 = Game.Content.Load<SoundEffect>("sfx/richter_huh_atk1");
            this.SomAtaque2 = Game.Content.Load<SoundEffect>("sfx/richter_yah_atk1");
            this.SofreuDano1 = Game.Content.Load<SoundEffect>("sfx/richter_oah_dano1");
            this.SofreuDano2 = Game.Content.Load<SoundEffect>("sfx/richter_ooh_dano2");
            this.SomAcaoPulo = Game.Content.Load<SoundEffect>("sfx/richter_superjump");
            this.Risada = Game.Content.Load<SoundEffect>("sfx/richter_risada");
            this.SomRasteira = Game.Content.Load<SoundEffect>("sfx/richter_rasteira");
            this.SomTeleport = Game.Content.Load<SoundEffect>("sfx/teleport");
            


        }
        //====================================================================================================================================================================================================
        public void Estados_Richter()
        {
            this.Frame.X += this.Tamanho.X;
            if (this.Estado == Estados.Idle)
            {
                if (this.Frame.X > 2 * this.Tamanho.X)
                    this.Frame.X = 0;
                this.Frame.Y = 0;
            }
            else if (this.Estado == Estados.Andando)
            {
                if (this.Frame.X > 6 * this.Tamanho.X)
                    this.Frame.X = 0;
                // this.SomPassos.Play();
            }

            else if (this.Estado == Estados.Rasteira)
            {
                if (this.Frame.X > 5 * this.Tamanho.X)
                    this.Frame.X = 0;
                this.SomRasteira.Play();
            }

            else if (this.Estado == Estados.AtaquePrimario)
            {
                if (this.Frame.X == this.FrameAnterior.X + Tamanho.X)
                {
                    this.SomAtaque1.Play();
                    this.SomCorte.Play();
                }
                if (this.Frame.X > 13 * this.Tamanho.X)
                {
                    this.Frame = this.FrameAnterior;
                    this.Atacando = false;
                    this.Estado = this.EstadoAnterior;
                }

            }

            else if (this.Estado == Estados.Correndo)
            {
                if (this.Frame.X > 7 * this.Tamanho.X)
                    this.Frame.X = 0;

            }
            else if (this.Estado == Estados.Agachado)
            {
                if (this.Frame.X > 0 * this.Tamanho.X)
                    this.Frame.X = 0;
            }
            else if (this.Estado == Estados.Parando)
            {
                if (this.Frame.X > 2 * this.Tamanho.X)
                    this.Frame.X = 0;
            }
            else if (this.Estado == Estados.PuloReto)
            {
                if (this.Frame.X > 0 * this.Tamanho.X)
                    this.Frame.X = 0;
            }
            else if (this.Estado == Estados.PuloInclinado)
            {
                if (this.Frame.X > 0 * this.Tamanho.X)
                    this.Frame.X = 0;
            }
            else if (this.Estado == Estados.Caindo)
            {
                if (this.Frame.X > 0 * this.Tamanho.X)
                    this.Frame.X = 0;
            }
            else if (this.Estado == Estados.Pulo_PontoMax)
            {
                if (this.Frame.X > 0 * this.Tamanho.X)
                    this.Frame.X = 0;
            }
            else if (this.Estado == Estados.Observando)
            {
                if (this.Frame.X > 3 * this.Tamanho.X)
                    this.Frame.X = 3 * this.Tamanho.X;

            }
            else if (this.Estado == Estados.SuperPulo)
            {
                if (this.Frame.X > 1 * this.Tamanho.X)
                    this.Frame.X = 0;

            }
            else if (this.Estado == Estados.Morto)
            {
                if (this.Frame.X > 0 * this.Tamanho.X)
                    this.Frame.X = 0;

            }
            else if (this.Estado == Estados.DanoEqueda)
            {
                if (this.Frame.X > 6 * this.Tamanho.X)
                    this.Frame.X = 0;

            }
            else if (this.Estado == Estados.ColisaoHorizontal)
            {
                if (this.Frame.X > 6 * this.Tamanho.X)
                    this.Frame.X = 0;
                this.Frame.Y = 0;
            }
            else if (this.Estado == Estados.EndGame)
            {
                if (this.Frame.X > 3 * this.Tamanho.X)
                    this.Frame.X = 0;
                this.Frame.Y = 0;
            }
        }
        //=========================================================================================================================================================================================================
        //Ações de Richter

        public void Check_Move_Richter()
        {
            if (Keyboard.GetState().IsKeyDown(Keys.Down))
                GolpeRasteiro = true;
            //if (Pulou == false && PulouMaisAlto == true)
            //    Pulou = false;
            if (Pulou == false && Atacando == false && PulouMaisAlto == false)
                Parado();
            if (Keyboard.GetState().IsKeyDown(Keys.Q))
                Atacar();
          
            //=====================================================
            //pulo
            //=============================================================================================================================================================
            if (Keyboard.GetState().IsKeyDown(Keys.Space) && Pulou == false && GolpeRasteiro == false)
            {
                Posicao.Y -= 15f;
                Velocidade.Y = -8f;
                Pulou = true;
                if (Keyboard.GetState().IsKeyDown(Keys.Right) || Keyboard.GetState().IsKeyDown(Keys.Left))
                {
                    PuloInclinado();
                    PulouInclinado = true;
                }
                else
                {
                    PuloReto();
                    PulouReto = true;
                }
                if (OrigemPulo.Y == 2 * Posicao.Y)
                    Pulo_PontoMax();
            }

            if (Pulou == true)
            {
                float i = 4;
                Velocidade.Y += 0.15f * i;

            }

            if (OrigemPulo.Y > 2 * Posicao.Y)
            {
                //PontoMaisAlto = true;
                Pulou = false; // começa a cair
                PontoMaisAlto = true;
                Pulando = true;
            }
            //if ((OrigemPulo.Y < 2 * Posicao.Y) && (Pulou == false))
            //{
            // Pulou = false;
            // Pulo_PontoMax();
            //    Caindo();
            //}


            if (Pulou == false) // saiu do chão
            {
                Velocidade.Y = 0f;
                if (PontoMaisAlto == true)
                {
                    Caindo();
                    Pulando = false;
                }
                if (PulouReto == true)
                {
                    Posicao.Y = OrigemPulo.Y;
                    PulouReto = false;
                }
                if (PulouInclinado == true)
                {
                    Posicao.Y = OrigemPulo.Y;
                    PulouInclinado = false;
                }
            }
            if (this.Estado != Estados.PuloReto)
                OrigemPulo = Posicao;
            if (this.Estado != Estados.PuloInclinado)
                OrigemPulo.Y = Posicao.Y;
            //=========================================================================================================================================================
            //SuperPulo
            /*
            if(Keyboard.GetState().IsKeyDown(Keys.B) && PulouMaisAlto == false)        
            {
                Posicao.Y -= 65f;
                Velocidade.Y = -18f;
                PulouMaisAlto = true;
                if (Keyboard.GetState().IsKeyDown(Keys.Right) || Keyboard.GetState().IsKeyDown(Keys.Left))
                {
                    SuperJump();
                    SuperPulo = true;
                }
                else
                {
                    SuperJump();
                    SuperPulo = true;
                }
                if (OrigemPulo.Y == 3 * Posicao.Y)
                    Pulo_PontoMax(); 
            }


            if (PulouMaisAlto == true)
            {
                float i = 5;
                Velocidade.Y += 0.25f * i;

            }
            if (OrigemPulo.Y > 3 * Posicao.Y)
            {
                //PontoMaisAlto = true;
                PulouMaisAlto = false; // começa a cair
                PontoMaisAlto = true;
                PulandoAlto = true;
            }
            if ((OrigemPulo.Y < 3 * Posicao.Y) && (PulouMaisAlto == false))
            {
                Caindo();
           }

            if (PulouMaisAlto == false) // saiu do chão
            {
                Velocidade.Y = 0f;
                if (PontoMaisAlto == true)
                {
                    Caindo();
                    //PulandoAlto = false;
                    SuperPulo = false;
                }
                if (SuperPulo == true)
                {
                    Posicao.Y = OrigemPulo.Y;
                    SuperPulo = false;
                }

            }

            if (this.Estado != Estados.SuperPulo)
                OrigemPulo = Posicao;
            */
            //=====================================================================================================================================================
            //Movimentos Andar e Correr
            if (Keyboard.GetState().IsKeyDown(Keys.Left))
            {
                if (Keyboard.GetState().IsKeyDown(Keys.RightShift))
                {
                    Movimento(Personagem.Direcoes.Esquerda);
                    Correr();

                }
                else
                {
                    Movimento(Personagem.Direcoes.Esquerda);
                    if (Pulou == false)
                        Andar();
                    else
                        PuloInclinado();
                }
            }
            else if (Keyboard.GetState().IsKeyDown(Keys.Right))
            {
                if (Keyboard.GetState().IsKeyDown(Keys.RightShift))
                {
                    Movimento(Personagem.Direcoes.Direita);
                    Correr();
                }
                else
                {
                    Movimento(Personagem.Direcoes.Direita);
                    if (Pulou == false)
                    {
                        Andar();
                    }
                    else
                        PuloInclinado();
                }

            }
            else if (Keyboard.GetState().IsKeyDown(Keys.Down))
            {
                Agachado(Personagem.Estados.Agachado);
                if (Keyboard.GetState().IsKeyDown(Keys.Space))
                {
                    GolpeRasteiro = true;
                    Rasteira();
                }
                GolpeRasteiro = false;
            }

            else if (Keyboard.GetState().IsKeyDown(Keys.Up))
                Observando(Personagem.Estados.Observando);
            else if (Pulou == false && PulouMaisAlto == false && Atacando == false)
            {
                Parado();
            }
            else if (Pulou == false && Atacando == true && PulouMaisAlto == false)
            {
                Atacar();
            }
        }
        public int Move_Screen()
        {
            if (OrigemPosicao.X >= Posicao.X + 1)
            {
                OrigemPosicao = Posicao;
                if (Textura == TexturaAndando)
                    return -3;
                if (Textura == TexturaRasteira)
                    return -6;
                else
                    return -5;
            }
            if (OrigemPosicao.X <= Posicao.X - 1)
            {
                OrigemPosicao = Posicao;
                if (Textura == TexturaAndando)
                    return +3;
                if (Textura == TexturaRasteira)
                    return +6;
                else
                    return +5;
            }
            else
            {
                return 0;
            }
        }
    }
}
