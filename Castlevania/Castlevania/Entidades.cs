﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using FuncWorks.XNA.XTiled;

namespace Castlevania
{
    public class Entidades : Microsoft.Xna.Framework.DrawableGameComponent
    {
        public enum Direcoes { Direita, Esquerda, Acima, Abaixo }
        public enum Estados { Movimentando }
        SpriteBatch spriteBatch;
        private Texture2D Textura1;
        private Texture2D Textura2;
        public int Vida;
        public int Vida1;
        public int Vida2;
        public int Vida3;
        public int Vida4;
        public int Vida5;
        public int Vida6;
        public int Vida7;
        public int Vida8;
        public int Vida9;
        public int Vida10;
        public int Vida11;
        public int Vida12;


        //public Vector2 Posicao;
        public Vector2 Velocidade;
        public Estados Estado;
        private Vector2 Frame;
        private Vector2 Frame2;
        //private Vector2 Tamanho;
        public Direcoes Direcao;
        public bool Vivo;
        public Vector2 Posicao1;
        public Vector2 Posicao2;
        public Vector2 Posicao3;
        public Vector2 Posicao4;
        public Vector2 Posicao5;
        public Vector2 Posicao6;
        public Vector2 Posicao7;
        public Vector2 Posicao8;
        public Vector2 Posicao9;
        public Vector2 Posicao10;
        public Vector2 Posicao11;
        public Vector2 Posicao12;
        public Vector2 Tamanho1;
        public Vector2 Tamanho2;
        public Rectangle BoundingBox2;
        public Rectangle BoundingBox1;
        public Rectangle BoundingBox3;
        public Rectangle BoundingBox4;
        public Rectangle BoundingBox5;
        public Rectangle BoundingBox6;
        public Rectangle BoundingBox7;
        public Rectangle BoundingBox8;
        public Rectangle BoundingBox9;
        public Rectangle BoundingBox10;
        public Rectangle BoundingBox11;
        public Rectangle BoundingBox12;
        public List<Vector2> Tamanho;
        public List<Vector2> Posicao;
        public List<Vector2> Inimigos;
        public List<Rectangle> BoundInimigos;
        public bool levoudano_ini = true;
        int max_positions = 1;
        public bool vivo;


        public Entidades(Game game)
            : base(game)
        {

            this.Posicao1 = new Vector2(300, 420);
            this.Posicao2 = new Vector2(390, 420);
            this.Posicao3 = new Vector2(390, 420);
            this.Posicao4 = new Vector2(500, 420);
            this.Posicao5 = new Vector2(530, 420);
            this.Posicao6 = new Vector2(700, 420);
            this.Posicao7 = new Vector2(1000, 420);
            this.Posicao8 = new Vector2(1200, 420);
            this.Posicao9 = new Vector2(1530, 490); //apartir daqui sao esqueletos
            this.Posicao10 = new Vector2(1600, 490);
            this.Posicao11 = new Vector2(1900, 490);
            this.Posicao12 = new Vector2(2000, 490);


            this.Velocidade = new Vector2(1, 1);
            this.Tamanho1 = new Vector2(26, 52);
            this.Tamanho2 = new Vector2(42, 47);
            this.Estado = Estados.Movimentando;
            this.Frame = new Vector2(0, 0);
            this.Frame2 = new Vector2(0, 0);
            this.Direcao = Direcoes.Direita;
            this.Vida = 100;
            this.vivo = true;
            //this.BoundingBox = new Rectangle((int)Posicao.X, (int)Posicao.Y, (int)Tamanho.X, (int)Tamanho.Y);
        }

        public void LoadContent(Game game)
        {
            LoadTexturesInimigos();
        }

        public override void Update(GameTime gameTime)
        {
            //Posicao += Velocidade;
            Estados_Inimigos();
            if (vivo == true)
                this.BoundingBox1 = new Rectangle((int)Posicao1.X, (int)Posicao1.Y, (int)Tamanho1.X, (int)Tamanho1.Y);
            this.BoundingBox2 = new Rectangle((int)Posicao2.X, (int)Posicao2.Y, (int)Tamanho1.X, (int)Tamanho1.Y);
            this.BoundingBox3 = new Rectangle((int)Posicao3.X, (int)Posicao3.Y, (int)Tamanho1.X, (int)Tamanho1.Y);
            this.BoundingBox4 = new Rectangle((int)Posicao4.X, (int)Posicao4.Y, (int)Tamanho1.X, (int)Tamanho1.Y);
            this.BoundingBox5 = new Rectangle((int)Posicao5.X, (int)Posicao5.Y, (int)Tamanho1.X, (int)Tamanho1.Y);
            this.BoundingBox6 = new Rectangle((int)Posicao6.X, (int)Posicao6.Y, (int)Tamanho1.X, (int)Tamanho1.Y);
            this.BoundingBox7 = new Rectangle((int)Posicao7.X, (int)Posicao7.Y, (int)Tamanho1.X, (int)Tamanho1.Y);
            this.BoundingBox8 = new Rectangle((int)Posicao8.X, (int)Posicao8.Y, (int)Tamanho1.X, (int)Tamanho1.Y);
            this.BoundingBox9 = new Rectangle((int)Posicao9.X, (int)Posicao9.Y, (int)Tamanho2.X, (int)Tamanho2.Y);
            this.BoundingBox10 = new Rectangle((int)Posicao10.X, (int)Posicao10.Y, (int)Tamanho2.X, (int)Tamanho2.Y);
            this.BoundingBox11 = new Rectangle((int)Posicao11.X, (int)Posicao11.Y, (int)Tamanho2.X, (int)Tamanho2.Y);
            this.BoundingBox12 = new Rectangle((int)Posicao12.X, (int)Posicao12.Y, (int)Tamanho2.X, (int)Tamanho2.Y);
            base.Update(gameTime);
        }

        public void Draw(GameTime gameTime, SpriteBatch spriteBatch, Rectangle mapView)
        {
            if (vivo == true)
            spriteBatch.Draw(this.Textura1, Map.Translate(Posicao1, mapView), new Rectangle((int)this.Frame.X, (int)this.Frame.Y, (int)this.Tamanho1.X, (int)this.Tamanho1.Y),
            Game1.playerColor, 0f, Vector2.Zero, 1f, this.Direcao == Direcoes.Direita ? SpriteEffects.None : SpriteEffects.FlipHorizontally, 0f);

            spriteBatch.Draw(this.Textura1, Map.Translate(Posicao2, mapView), new Rectangle((int)this.Frame.X, (int)this.Frame.Y, (int)this.Tamanho1.X, (int)this.Tamanho1.Y),
            Game1.playerColor, 0f, Vector2.Zero, 1f, this.Direcao == Direcoes.Direita ? SpriteEffects.None : SpriteEffects.FlipHorizontally, 0f);

            spriteBatch.Draw(this.Textura1, Map.Translate(Posicao3, mapView), new Rectangle((int)this.Frame.X, (int)this.Frame.Y, (int)this.Tamanho1.X, (int)this.Tamanho1.Y),
            Game1.playerColor, 0f, Vector2.Zero, 1f, this.Direcao == Direcoes.Direita ? SpriteEffects.None : SpriteEffects.FlipHorizontally, 0f);

            spriteBatch.Draw(this.Textura1, Map.Translate(Posicao4, mapView), new Rectangle((int)this.Frame.X, (int)this.Frame.Y, (int)this.Tamanho1.X, (int)this.Tamanho1.Y),
            Game1.playerColor, 0f, Vector2.Zero, 1f, this.Direcao == Direcoes.Direita ? SpriteEffects.None : SpriteEffects.FlipHorizontally, 0f);

            spriteBatch.Draw(this.Textura1, Map.Translate(Posicao5, mapView), new Rectangle((int)this.Frame.X, (int)this.Frame.Y, (int)this.Tamanho1.X, (int)this.Tamanho1.Y),
            Game1.playerColor, 0f, Vector2.Zero, 1f, this.Direcao == Direcoes.Direita ? SpriteEffects.None : SpriteEffects.FlipHorizontally, 0f);

            spriteBatch.Draw(this.Textura1, Map.Translate(Posicao6, mapView), new Rectangle((int)this.Frame.X, (int)this.Frame.Y, (int)this.Tamanho1.X, (int)this.Tamanho1.Y),
            Game1.playerColor, 0f, Vector2.Zero, 1f, this.Direcao == Direcoes.Direita ? SpriteEffects.None : SpriteEffects.FlipHorizontally, 0f);

            spriteBatch.Draw(this.Textura1, Map.Translate(Posicao7, mapView), new Rectangle((int)this.Frame.X, (int)this.Frame.Y, (int)this.Tamanho1.X, (int)this.Tamanho1.Y),
            Game1.playerColor, 0f, Vector2.Zero, 1f, this.Direcao == Direcoes.Direita ? SpriteEffects.None : SpriteEffects.FlipHorizontally, 0f);

            spriteBatch.Draw(this.Textura1, Map.Translate(Posicao8, mapView), new Rectangle((int)this.Frame.X, (int)this.Frame.Y, (int)this.Tamanho1.X, (int)this.Tamanho1.Y),
            Game1.playerColor, 0f, Vector2.Zero, 1f, this.Direcao == Direcoes.Direita ? SpriteEffects.None : SpriteEffects.FlipHorizontally, 0f);

            spriteBatch.Draw(this.Textura2, Map.Translate(Posicao9, mapView), new Rectangle((int)this.Frame2.X, (int)this.Frame2.Y, (int)this.Tamanho2.X, (int)this.Tamanho2.Y),
            Game1.playerColor, 0f, Vector2.Zero, 1f, this.Direcao == Direcoes.Direita ? SpriteEffects.None : SpriteEffects.FlipHorizontally, 0f);

            spriteBatch.Draw(this.Textura2, Map.Translate(Posicao10, mapView), new Rectangle((int)this.Frame2.X, (int)this.Frame2.Y, (int)this.Tamanho2.X, (int)this.Tamanho2.Y),
            Game1.playerColor, 0f, Vector2.Zero, 1f, this.Direcao == Direcoes.Direita ? SpriteEffects.None : SpriteEffects.FlipHorizontally, 0f);

            spriteBatch.Draw(this.Textura2, Map.Translate(Posicao11, mapView), new Rectangle((int)this.Frame2.X, (int)this.Frame2.Y, (int)this.Tamanho2.X, (int)this.Tamanho2.Y),
            Game1.playerColor, 0f, Vector2.Zero, 1f, this.Direcao == Direcoes.Direita ? SpriteEffects.None : SpriteEffects.FlipHorizontally, 0f);

            spriteBatch.Draw(this.Textura2, Map.Translate(Posicao12, mapView), new Rectangle((int)this.Frame2.X, (int)this.Frame2.Y, (int)this.Tamanho2.X, (int)this.Tamanho2.Y),
            Game1.playerColor, 0f, Vector2.Zero, 1f, this.Direcao == Direcoes.Direita ? SpriteEffects.None : SpriteEffects.FlipHorizontally, 0f);

            base.Draw(gameTime);
        }
        public void LoadTexturesInimigos()
        {
            this.Textura1 = Game.Content.Load<Texture2D>("sprites_enemys/morcego");
            this.Textura2 = Game.Content.Load<Texture2D>("sprites_enemys/Blood_Skeleton");
        }

        public void Estados_Inimigos()
        {

            this.Frame.X += this.Tamanho1.X;
            if (this.Estado == Estados.Movimentando)
            {
                if (this.Frame.X > 7 * this.Tamanho1.X)
                    this.Frame.X = 0;
                this.Frame.Y = 0;
            }
            
            this.Frame2.X += this.Tamanho2.X;
            if (this.Estado == Estados.Movimentando)
            {
                if (this.Frame2.X > 6 * this.Tamanho2.X)
                    this.Frame2.X = 0;
                this.Frame2.Y = 0;
            }
             
        }
        public void Movimento()
        {
            this.Estado = Estados.Movimentando;
        }

        public void SofrerDano(int d)
        {
            levoudano_ini = true;
            //this.Estado = Estados.DanoEqueda;
            this.Vida -= d;
            //this.Posicao.X = this.Posicao.X - 4;
            levoudano_ini = false;
            if (Vida == 0)
                vivo = false;
        }

        public void Movimento_Skeleton()
        {
            this.MudarVelocidade(2f);
            this.Posicao9.X -= this.Velocidade.X;
            if (Posicao9.X == 1250)
            {
                this.Posicao9.X += this.Velocidade.X;

            }
            if(Posicao9.X == 1550)
                this.Posicao9.X -= this.Velocidade.X;
     

        }
        public void MudarVelocidade(float v)
        {
            this.Velocidade.X = v;
        }

    }
}
