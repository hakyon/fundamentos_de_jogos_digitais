﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;

namespace Castlevania
{
    public class Camera
    {

        Matrix viewMatrix;
        Vector2 position;
        float scale = 1.0f;

        public Matrix ViewMatrix
        {
            get { return viewMatrix; }
        }

        public Rectangle Update(Personagem Richter, Rectangle view)
        {

            position.X = ((Richter.Posicao.X + 16) - (view.Width / 2) / scale);
            position.Y = ((Richter.Posicao.Y + 24) - (view.Height / 2) / scale);

            if (position.X < 0)
                position.X = 0;

            if (position.Y < 0)
                position.Y = 0;
            if (Keyboard.GetState().IsKeyDown(Keys.Z))
                scale += 0.10f;
            else if (Keyboard.GetState().IsKeyDown(Keys.X))
                scale -= 0.10f;

            viewMatrix = Matrix.CreateTranslation(new Vector3(-position, 0)) * Matrix.CreateScale(scale);

            return view;
        }


    }
}
