using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using FuncWorks.XNA.XTiled;
using System.Threading;


namespace Castlevania
{

    public class Game1 : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        Personagem Richter;
        Sleep Sleep;
        public Rectangle BoundingBoxB;
        public ObjectLayer layer;
        public Vector2 PosicaoInicial;
        public Rectangle spriteRectangle;
        public Vector2 spritePosition;
        const float VelocidadeTangencial = 5f;
        Camera Camera;
        int TelaSizeX = 800;
        int TelaSizeY = 600;
        Entidades entidades;

        float variacao_tela;
        Rectangle mapView;
        Int32 mapIdx;
        List<Map> maps;
        public static Color playerColor;
        private Song SomMenu;
        private Song SomFase1;
        TimeSpan morreu;
        Timer tempo_morte;


        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            graphics.PreferredBackBufferHeight = TelaSizeY;
            graphics.PreferredBackBufferWidth = TelaSizeX;
        }

        protected override void Initialize()
        {
            mapView = graphics.GraphicsDevice.Viewport.Bounds;
            mapView.X = 0;
            mapView.Y = 0;


            //Richter.BoundingBox = maps[mapIdx].SourceTiles[0].Source;
            //tempo_morte = new Timer(4);
            morreu = new TimeSpan();
            playerColor = Color.White;
            Sleep = new Sleep();
            Camera = new Camera();
            Richter = new Personagem(this);
            entidades = new Entidades(this);
            Random r = new Random();
            this.Window.Title = "Castlevania - Ascenssion of Darkness (FanGame) - Alan";
            Richter.Initialize();
            base.Initialize();

        }

        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);
            Map.InitObjectDrawing(graphics.GraphicsDevice);
            maps = new List<Map>();
            //maps.Add(Content.Load<Map>("Levels/Mapa6"));
            maps.Add(Content.Load<Map>("Levels/Mapa12"));
            //maps.Add(Content.Load<Map>("Levels/Mapa6"));
            Richter.LoadContent(this);
            entidades.LoadContent(this);
            LoadSoundGame();
            mapIdx = 0;
            //MediaPlayer.Play(SomFase1);
            //MediaPlayer.IsRepeating = true;

        }
        protected override void UnloadContent()
        {

        }

        protected override void Update(GameTime gameTime)
        {
            Richter.Posicao.Y = +Richter.Posicao.Y;

            Viewport viewScreen;
            viewScreen = graphics.GraphicsDevice.Viewport;
            Rectangle delta = mapView;
            this.TargetElapsedTime = TimeSpan.FromSeconds(1.0f / 15.0f);

            if (Richter.Esta_no_solo == true)
                if (Keyboard.GetState().IsKeyDown(Keys.Q))
                    Richter.Atacar();

            //if (Richter.Esta_no_solo == false && Richter.Esta_morto == false)
            //     Richter.Caindo();
            if (Richter.Atacando == false && Richter.Pulando == false && Richter.GolpeRasteiro == false && Richter.levoudano == false && Richter.Esta_morto == false && Richter.PulandoAlto == false)// && Richter.Esta_no_solo == true) 
            {
                Richter.Check_Move_Richter();
                if (Richter.Atacando)
                    this.TargetElapsedTime = TimeSpan.FromSeconds(1.0f / 18.0f);
            }

            Richter.Update(gameTime);
            entidades.Update(gameTime);

            if (Richter.colidiu_no_comeco == true)
                mapView.X = 0;


            //checagem de dano e morte
            if (entidades.Vivo == true)
            {
                this.TargetElapsedTime = TimeSpan.FromSeconds(1.0f / 20.0f);
                entidades.Movimento();
            }
            if (Richter.BoundingBox.Intersects(entidades.BoundingBox1) && Richter.Atacando == false)
                Richter.SofrerDano(10);
            if (Richter.BoundingBox.Intersects(entidades.BoundingBox2) && Richter.Atacando == false)
                Richter.SofrerDano(10);
       /*     if (Richter.BoundingBox.Intersects(entidades.BoundingBox3) && Richter.Atacando == false)
                Richter.SofrerDano(10);
            if (Richter.BoundingBox.Intersects(entidades.BoundingBox4) && Richter.Atacando == false)
                Richter.SofrerDano(10);
            if (Richter.BoundingBox.Intersects(entidades.BoundingBox5) && Richter.Atacando == false)
                Richter.SofrerDano(10);
            if (Richter.BoundingBox.Intersects(entidades.BoundingBox6) && Richter.Atacando == false)
                Richter.SofrerDano(10);
            if (Richter.BoundingBox.Intersects(entidades.BoundingBox7) && Richter.Atacando == false)
                Richter.SofrerDano(10);
            if (Richter.BoundingBox.Intersects(entidades.BoundingBox8) && Richter.Atacando == false)
                Richter.SofrerDano(10);
            if (Richter.BoundingBox.Intersects(entidades.BoundingBox9) && Richter.Atacando == false)
                Richter.SofrerDano(10);
            if (Richter.BoundingBox.Intersects(entidades.BoundingBox10) && Richter.Atacando == false)
                Richter.SofrerDano(10);
            if (Richter.BoundingBox.Intersects(entidades.BoundingBox11) && Richter.Atacando == false)
                Richter.SofrerDano(10);
            if (Richter.BoundingBox.Intersects(entidades.BoundingBox12) && Richter.Atacando == false)
                Richter.SofrerDano(10); */
            if (Richter.Vida == 0)
                Richter.Morto();
            if (Richter.Esta_morto == true)
            {

                Richter.Textura = Richter.TexturaMorte;
                Richter.Posicao = Richter.OrigemPosicao; // permanece morto onde foi morto
                //string n1 = morreu.ToString("00:00");  

                //while
                //    morreu != morreu + 2;
                Initialize();
            }
            //checagem de dano das entidades

            if (Richter.BondingBoxAtaque.Intersects(entidades.BoundingBox1) && Richter.Atacando == true)
                entidades.SofrerDano(30);
            if (Richter.BondingBoxAtaque.Intersects(entidades.BoundingBox2) && Richter.Atacando == true)
                entidades.SofrerDano(30);
            if (Richter.BondingBoxAtaque.Intersects(entidades.BoundingBox3) && Richter.Atacando == true)
                entidades.SofrerDano(30);
         /*   if (Richter.BondingBoxAtaque.Intersects(entidades.BoundingBox4) && Richter.Atacando == true)
                entidades.SofrerDano(30);
            if (Richter.BondingBoxAtaque.Intersects(entidades.BoundingBox5) && Richter.Atacando == true)
                entidades.SofrerDano(5);
            if (Richter.BondingBoxAtaque.Intersects(entidades.BoundingBox6) && Richter.Atacando == true)
                entidades.SofrerDano(5);
            if (Richter.BondingBoxAtaque.Intersects(entidades.BoundingBox7) && Richter.Atacando == true)
                entidades.SofrerDano(5);
            if (Richter.BondingBoxAtaque.Intersects(entidades.BoundingBox8) && Richter.Atacando == true)
                entidades.SofrerDano(5);
            if (Richter.BondingBoxAtaque.Intersects(entidades.BoundingBox9) && Richter.Atacando == true)
                entidades.SofrerDano(5);
            if (Richter.BondingBoxAtaque.Intersects(entidades.BoundingBox10) && Richter.Atacando == true)
                entidades.SofrerDano(5);
            if (Richter.BondingBoxAtaque.Intersects(entidades.BoundingBox11) && Richter.Atacando == true)
                entidades.SofrerDano(5);
            if (Richter.BondingBoxAtaque.Intersects(entidades.BoundingBox12) && Richter.Atacando == true)
                entidades.SofrerDano(5); */
            if (entidades.Vida == 0)
                entidades.vivo = false;

            //checa se chegou ao fim da fase

            if(Richter.Posicao.X >= 2364) //fim da faxe fixo no tamanho do mapa
            {
                Richter.EndGame_Reset();
                Initialize();
            }
            //Teste de Colis�o usando BoundingBox da back
            CheckIntersectLevel();

            Camera.Update(Richter, mapView);

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            //Viewport view;
            GraphicsDevice.Clear(Color.Black);

            //if (mapIdx == 8)
            //    spriteBatch.Begin(SpriteSortMode.Immediate, BlendState.NonPremultiplied);
            //else
            spriteBatch.Begin();


            //area responsavel por movimentar a tela em fun��o ao deslocamento do personagem
            variacao_tela = Richter.Move_Screen();
            float variacao_tela_back = Math.Abs(variacao_tela);
            if (variacao_tela < 0)
            {
                Richter.BoundingBox = new Rectangle((int)Richter.Posicao.X, (int)Richter.Posicao.Y, (int)Richter.Tamanho.X, (int)Richter.Tamanho.Y);
                Richter.BondingBoxAtaque = new Rectangle((int)Richter.Posicao.X + 50, (int)Richter.Posicao.Y - 50, (int)Richter.PontoAtaque.X, (int)Richter.PontoAtaque.Y);
                if (mapView.X - variacao_tela_back <= 0)
                    variacao_tela = 0;
                else
                    mapView.X = mapView.X - (int)variacao_tela_back;
            }
            if (variacao_tela > 0)
            {
                mapView.X = mapView.X + (int)variacao_tela;
                Richter.BoundingBox = new Rectangle((int)Richter.Posicao.X, (int)Richter.Posicao.Y, (int)Richter.Tamanho.X, (int)Richter.Tamanho.Y);
                Richter.BondingBoxAtaque = new Rectangle((int)Richter.Posicao.X + 50, (int)Richter.Posicao.Y - 50, (int)Richter.PontoAtaque.X, (int)Richter.PontoAtaque.Y);
            }

            maps[mapIdx].Draw(spriteBatch, mapView);

            //funcoes para testar colisoes de boundigbox do personagem e mapa
            // desenha a boundinBox do mapa
            for (int ol = 0; ol < maps[mapIdx].ObjectLayers.Count; ol++)
                maps[mapIdx].DrawObjectLayer(spriteBatch, ol, mapView, 1.0f);

            // draw a boundingBox do personagem para que mude de cor quando colidir com algo
            //spriteBatch.Draw(maps[mapIdx].Tilesets[maps[mapIdx].SourceTiles[0].TilesetID].Texture,
            //                 Map.Translate(Richter.BoundingBox, mapView),
            //                 maps[mapIdx].SourceTiles[0].Source,
            //                 playerColor);

            //desenha a boundingBox do personagem
            //spriteBatch.Draw(maps[mapIdx].Tilesets[maps[mapIdx].SourceTiles[0].TilesetID].Texture,
            //    Map.Translate(Richter.BoundingBox, mapView),
            //    maps[mapIdx].SourceTiles[0].Source, playerColor);

            // desenho caixa de colisao de atk
            spriteBatch.Draw(maps[mapIdx].Tilesets[maps[mapIdx].SourceTiles[0].TilesetID].Texture,
            Map.Translate(Richter.PontoAtaque, mapView),
            maps[mapIdx].SourceTiles[0].Source, playerColor);

            Richter.Draw(gameTime, spriteBatch, mapView);
            entidades.Draw(gameTime, spriteBatch, mapView);
            spriteBatch.End();
            base.Draw(gameTime);
        }

        //=============================================================================================================
        //Interce��es dentro do cenario carregado
        public void LoadSoundGame()
        {
            this.SomFase1 = Content.Load<Song>("sound/Fase01");
            this.SomMenu = Content.Load<Song>("sound/MainMenu");
        }
        public void CheckIntersectLevel()
        {
            //Verifica colisao com as boundingBox do Personagem na fase 01
            Rectangle delta = mapView;

            if (Richter.Posicao.X <= 0)
                Richter.Posicao.X = Richter.Posicao.X = Richter.OrigemPosicao.X;

            if (Richter.Posicao.X >= 2366) //valor setado definido pelo tamanho da largura do mapa em TiledMap
                Richter.Posicao.X = Richter.Posicao.X = Richter.OrigemPosicao.X;

            if (maps[mapIdx].Bounds.Contains(delta))
            {
                Richter.Posicao.X += delta.X - mapView.X;
                Richter.Posicao.Y += delta.Y - mapView.Y;
                mapView = delta;
            }
            playerColor = Color.White;
            for (int ol = 0; ol < maps[mapIdx].ObjectLayers.Count; ol++)
            {
                for (int o = 0; o < maps[mapIdx].ObjectLayers[ol].MapObjects.Length; o++)
                {
                    if (maps[mapIdx].ObjectLayers[ol].MapObjects[o].Polygon != null)
                    {
                        if (maps[mapIdx].ObjectLayers[ol].MapObjects[o].Polygon.Contains(ref Richter.BoundingBox))
                        {
                            playerColor = Color.Red;
                            Richter.Colidindo_na_horizontal();
                        }
                        else if (maps[mapIdx].ObjectLayers[ol].MapObjects[o].Polygon.Intersects(ref Richter.BoundingBox))
                        {
                            //playerColor = Color.Yellow;//colisao caindo diagonal
                            Richter.Colidindo_na_vertical();
                        }

                    }
                    if (maps[mapIdx].ObjectLayers[ol].MapObjects[o].Polyline != null) // colisao 
                    {
                        if (maps[mapIdx].ObjectLayers[ol].MapObjects[o].Polyline.Intersects(ref Richter.BoundingBox))
                        {
                            //playerColor = Color.Yellow;
                            Richter.Colidindo_na_horizontal();
                        }
                        else if (maps[mapIdx].ObjectLayers[ol].MapObjects[o].Polyline.Intersects(ref Richter.BoundingBox))
                        {
                            //playerColor = Color.Yellow;//colisao caindo diagonal
                            Richter.Colidindo_na_vertical();

                        }
                    }
                }

            }
        }
        // next

    }

    /*public class Sleep
    {
        public void msleep()
        {
            for (int i = 0; i < 5; i++)
            {
                Console.WriteLine("Sleep for 2 seconds.");
                Thread.Sleep(2000);
            }

            Console.WriteLine("Main thread exits.");
        }
    }*/
    public class Sleep
    {
        public void msleep()
        {
            Thread thrd1 = new Thread(new ThreadStart(Trmain));
            thrd1.Start();
        }
        public void Trmain()
        {
            for (; ; )
            {
                Console.WriteLine("waiting 1 minutes...");
                Thread.Sleep(1000 * 60 * 1);
            }
        }
    }

    public class Menu
    {
        /*   
           enum GameState { StartMenu, Loading, Playing, Paused }
           private Texture2D orb;
           private Texture2D startButton;
           private Texture2D exitButton;
           private Texture2D pauseButton;
           private Texture2D resumeButton;
           private Texture2D loadingScreen;
           private Vector2 orbPosition;
           private float speed = 1.5f;
           private Vector2 startButtonPosition;
           private Vector2 exitButtonPosition;
           private Vector2 resumeButtonPosition;
           private const float OrbWidth = 50f;
           private const float OrbHeight = 50f;
           //private float speed = 1.5f;
         
           //private Thread backgroundThread;
           bool IsMouseVisible = true;
           SpriteBatch spritebatch;
           public GameState gameState;

           private Thread backgroundThread;
           private bool isLoading = false;
           MouseState mouseState;
           MouseState previousMouseState;

           public void OpLoadGame(Game game)
           {

               orb = game.Content.Load<Texture2D>("icon/bati_icon");
               orbPosition = new Vector2((game.GraphicsDevice.Viewport.Width / 2) - (OrbWidth / 2),
                        (game.GraphicsDevice.Viewport.Height / 2) - (OrbHeight / 2));

               Thread.Sleep(3000);

               gameState = GameState.Playing;
               isLoading = true;
           }

           void Initialize(Game1 game)
           {
               IsMouseVisible = true;
               OpLoadGame(game);

               startButtonPosition = new Vector2((game.GraphicsDevice.Viewport.Width / 2) - 50, 200);
               exitButtonPosition = new Vector2((game.GraphicsDevice.Viewport.Width / 2) - 50, 250);
            
               gameState = GameState.StartMenu;
               //base.Initialize();
           }



           public void Draw(GameTime gameTime, Game game)
           {
               game.GraphicsDevice.Clear(Color.CornflowerBlue);
               spritebatch.Begin();
               if (gameState == GameState.StartMenu)
               {
                   spritebatch.Draw(startButton, startButtonPosition, Color.White);
                   spritebatch.Draw(exitButton, exitButtonPosition, Color.White);
               }
               if (gameState == GameState.Playing)
               {
                   spritebatch.Draw(orb, orbPosition, Color.White);
               }
               //draw the orb
               spritebatch.End();

               //base.Draw(gameTime);

           }
           protected override void Update(GameTime gameTime , Game game)
           {
               //if Keyboard.GetState().IsKeyDown(Keys.Escape))
               //    
               orbPosition.X += speed;


               if (gameState == GameState.Loading && !isLoading)
               {
                   backgroundThread = new Thread();
               }
               if (orbPosition.X > (game.GraphicsDevice.Viewport.Width - OrbWidth) || orbPosition.X < 0)
               {
                   speed *= -1;
               }
               mouseState = Mouse.GetState();
               if (previousMouseState.LeftButton == ButtonState.Pressed && mouseState.LeftButton == ButtonState.Released)
               {
                   MouseClicked(mouseState.X, mouseState.Y);
               }

               if (gameState == GameState.Playing && isLoading)
               {
                   OpLoadGame(game);
                   isLoading = false;
               }

               //base.Update(gameTime);
           }
           protected override void LoadContent(Game game)
           {
               spritebatch = new SpriteBatch(game.GraphicsDevice);
               startButton = game. Content.Load<Texture2D>(@"start");
               exitButton = game.Content.Load<Texture2D>(@"exit");
               loadingScreen = game.Content.Load<Texture2D>(@"loading");
           }

           void MouseClicked(int x, int y)
           {
               Rectangle mouseClickRect = new Rectangle(x, y, 10, 10);

               if (gameState == GameState.StartMenu)
               {
                   Rectangle startButtonRect = new Rectangle((int)startButtonPosition.X, (int)startButtonPosition.Y, 100, 20);
                   Rectangle exitButtonRect = new Rectangle((int)exitButtonPosition.X, (int)exitButtonPosition.Y, 100, 20);

                   if (mouseClickRect.Intersects(startButtonRect)) //player clicked start button
                   {
                       gameState = GameState.Playing;
                       isLoading = true;
                   }
                   else if (mouseClickRect.Intersects(exitButtonRect)) //player clicked exit button
                   {
                    
                   }
               }
           } */

    }

}
