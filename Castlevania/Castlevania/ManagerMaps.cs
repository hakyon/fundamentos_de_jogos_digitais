﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using FuncWorks.XNA.XTiled;


namespace Castlevania
{
    public class ManagerMaps : Microsoft.Xna.Framework.DrawableGameComponent
    {
        SpriteBatch spriteBatch;
        //public Vector2 Posicao;
        public Texture2D backGroundTexture;


        Rectangle mapView;
        Int32 mapIdx;
        List<Map> maps;
        public static Color playerColor;
        GraphicsDeviceManager graphics;

        public ManagerMaps(Game game)
            : base(game)
        {
            //graphics = new GraphicsDeviceManager(game);

            // this.BoundingBoxScenario01 = new Rectangle((int)Posicao.X, (int)Posicao.Y, (int)Tamanho.X, (int)Tamanho.Y);
            //Content.RootDirectory = "Content";
        }

        public override void Initialize()
        {
            playerColor = Color.White;
            mapView = graphics.GraphicsDevice.Viewport.Bounds;
            mapView.X = 0;
            mapView.Y = 0;

            base.Initialize();
        }

        public void LoadContent(Game game)
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);
            Map.InitObjectDrawing(game.GraphicsDevice);

            maps = new List<Map>();
            maps.Add(Game.Content.Load<Map>("Levels/Mapa1"));
            mapIdx = 0;

            base.LoadContent();

        }

        public override void Update(GameTime gameTime)
        {
            //this.BoundingBoxScenario01 = new Rectangle((int)Posicao.X, (int)Posicao.Y, (int)Tamanho.X, (int)Tamanho.Y);
            Rectangle delta = mapView;
            //CheckIntersectLevel(Personagem Richter);

            base.Update(gameTime);
        }

        public void Draw(GameTime gameTime,SpriteBatch spriteBatch)
        {

            //spriteBatch.Begin();

            maps[mapIdx].Draw(spriteBatch, mapView);

            for (int ol = 0; ol < maps[mapIdx].ObjectLayers.Count; ol++)
                maps[mapIdx].DrawObjectLayer(spriteBatch, ol, mapView, 1.0f);

            ///spriteBatch.End();
            base.Draw(gameTime);

        }

        public void LoadTexturesPhase01()
        {
            backGroundTexture = Game.Content.Load<Texture2D>("background/piso_teste");

        }

        public void CheckIntersectLevel(Personagem Richter)
        {
            //Verifica colisao com as boundingBox do Personagem na fase 01
            Rectangle delta = mapView;
            
            if (maps[mapIdx].Bounds.Contains(delta))
            {
                Richter.Posicao.X += delta.X - mapView.X;
                Richter.Posicao.Y += delta.Y - mapView.Y;
                mapView = delta;
            }
            


            playerColor = Color.White;
            for (int ol = 0; ol < maps[mapIdx].ObjectLayers.Count; ol++)
            {
                for (int o = 0; o < maps[mapIdx].ObjectLayers[ol].MapObjects.Length; o++)
                {
                    if (maps[mapIdx].ObjectLayers[ol].MapObjects[o].Polygon != null)
                    {
                        if (maps[mapIdx].ObjectLayers[ol].MapObjects[o].Polygon.Contains(ref Richter.BoundingBox))
                        {
                            playerColor = Color.Red;
                        }
                        else if (maps[mapIdx].ObjectLayers[ol].MapObjects[o].Polygon.Intersects(ref Richter.BoundingBox))
                        {
                            playerColor = Color.Yellow;
                            Richter.Colidindo_na_horizontal();
                        }
                    }
                    if (maps[mapIdx].ObjectLayers[ol].MapObjects[o].Polyline != null) // colisao 
                    {
                        if (maps[mapIdx].ObjectLayers[ol].MapObjects[o].Polyline.Intersects(ref Richter.BoundingBox))
                        {
                            playerColor = Color.Yellow;
                            Richter.Colidindo_na_horizontal();
                        }
                    }
                }

            }
        }

    }
}
