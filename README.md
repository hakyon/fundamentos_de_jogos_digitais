### Universidade Tuiuti do Parana ####
### Ciência da Computação ###
### Disciplina: Fundamentos de Jogos Digitais ###

### Exercicios/Trabalhos ###

* Periodo : 06/2015 - 12/2015
* Ambiente : Desktop

### Ferramentas usadas ###

* Linguagem: C#
* Framework : XNA ,XTiled
* IDE: Visual Studio 2015
* Plataforma : Windows

### Assuntos  ###
* Conceitos basicos para construção de um jogo em 2D
* Estrutura de documentação do projeto

### Autor  ###

* Alan Azevedo Bancks

### Contato ###

* E-mail: dsalan@hotmail.com
